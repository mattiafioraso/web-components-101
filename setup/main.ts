import { defineAppSetup } from '@slidev/types'

import '../components/moka-pot.js';
import '../components/moka-pot-shadow.js';
import '../components/moka-pot-template.js';


const template = document.createElement('template');

template.id = 'moka-template';
template.innerHTML = `
  <style>
    header {
      color: yellow;
      text-align: center;
    }
    main {
      text-align: center;
    }
  </style>
  
  <header>
    <h3>Moka Pot</h3>
  </header>

  <main></main>

  <slot></slot>
`;

document.body.appendChild(template)

export default defineAppSetup(() => {});
