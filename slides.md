---
# try also 'default' to start simple
theme: default
# random image from a curated Unsplash collection by Anthony
# like them? see https://unsplash.com/collections/94734566/slidev
background: https://source.unsplash.com/TD4DBagg2wE
# apply any windi css classes to the current slide
class: "text-center dark"
# https://sli.dev/custom/highlighters.html
highlighter: shiki
# show line numbers in code blocks
lineNumbers: false
# some information about the slides, markdown enabled
info: |
  ## Slidev Starter Template
  Presentation slides for developers.

  Learn more at [Sli.dev](https://sli.dev)
# persist drawings in exports and build
drawings:
  persist: false
---

# Web ~~Coffee~~ Components 101

What are they? And why do we want to use them?

<!--
The last comment block of each slide will be treated as slide notes. It will be visible and editable in Presenter Mode along with the slide. [Read more in the docs](https://sli.dev/guide/syntax.html#notes)
-->

---
layout: center
---

# What are Web Components?

According to MDN:

> Web Components is a suite of different technologies allowing you to create reusable custom elements — with their functionality encapsulated away from the rest of your code — and utilize them in your web apps.

The three main technologies are:

- 🎨 **Custom elements** - A set of JavaScript APIs that allow us to define elements that extend the HTML tag set and their behavior, which can then be used as desired in our UI.
- 🥷 **Shadow DOM** - A set of JavaScript APIs for attaching an encapsulated (so called "shadow") DOM tree to an element of our choice, which is rendered separately from the main document DOM.
- 📜 **HTML templates** - The `<template>` and `<slot>` elements enable us to write reusable markup templates that are not displayed in the rendered page.

<!--
- 🎨 **Custom elements** - They can then be used as desired in our UI.
- 🥷 **Shadow DOM** - In this way, we can keep an element's features private, so they can be scripted and styled avoiding any sort of collision with other parts of the document.
- 📜 **HTML templates** - These can be the basis of a custom element's structure.
-->

---
layout: section
---

# Custom Elements

---
layout: center
---

# Define a Custom Element

The minimum requirement to create a Web Component is to define a **Custom Element**, this can be achieved by:

- Implementing a class describing the desired functionality, using the `class` syntax we're familiar with.
- Register our new class in the `CustomElementRegistry` provided by the browser, using its `.define()` method.

There are two types of custom elements:

- **Autonomous custom elements** - These are completely new HTML elements, we can use them by simply writing them as `<hello-world>,` or `document.createElement("hello-world")`
- **Customized built-in elements** - These inherit from existing HTML elements, we can use them by writing the base element and specifying out custom name in the `is` attribute, like `<p is="word-count">`, or `document.createElement("p", { is: "word-count" })`.

<!--
  Not all browsers support customized built-ins (ahem Safari), so we'll leave them out of this presentation and we'll stick to autonomous ones.
-->

---
layout: image-right
image: https://source.unsplash.com/mn2tsPe6Oe8
---

# Implementing a `moka-pot`

Let's implement our first `MokaPot` custom element, that will:

- Display a **"Make Coffee"** button
- On button click:
  - Hide the button and show a _"Making Coffee..."_ placeholder
  - Wait some time, based on servings (which should be configurable via the `servings` attribute)
  - Display ☕️ emojis, based on servings
  - On emoji click, reset state

---
layout: two-cols
clicks: 2
---

# Implementation

```ts {all|1|3|all}
class MokaPot extends HTMLElement {
  constructor() {
    super();

    // ...
  }
}
```

::right::

Here we declare our class that will define how our custom element will behave

<v-clicks at="1">

- Our class must extend the `HTMLElement` class, which is the base class that all elements extend
- Always remember to call `super()` to construct the parent class

</v-clicks>

<style>
  .two-columns {
    gap: 1em;
    align-items: center;
  }
</style>

---
layout: two-cols
clicks: 4
---

# Adding some content

```ts {all|2|3|4|2-4}
class MokaPot extends HTMLElement {
  button = document.createElement("button");
  placeholder = document.createElement("span");
  cups = document.createElement("span");

  constructor() {
    super();

    // ...
  }
}
```

::right::

We can now add some content to the element

<v-clicks at="1">

- `button` - The "Make Coffee" button
- `placeholder` - A loading state placeholder
- `cups` - Our freshly brewed cups of coffee

</v-clicks>

<v-click at="4">

<br />
<blockquote>
Note that these elements are just created and stored in the class instance, they're not added to the DOM yet.
</blockquote>

</v-click>

<style>
  .two-columns {
    gap: 1em;
    align-items: center;
  }
</style>

---
layout: center
clicks: 1
---

# Customizing children and first render

Here we add some text on each child

```ts {9-11|14-16}
class MokaPot extends HTMLElement {
  button = document.createElement("button");
  placeholder = document.createElement("span");
  cups = document.createElement("i");

  constructor() {
    super();

    this.button.innerText = "Make Coffee";
    this.placeholder.innerText = "Making Coffee...";
    this.cups.innerText = "☕️";
  }

  connectedCallback() {
    this.replaceChildren(this.button);
  }
}

```

<v-click at="1">

And we render our initial state on `connectedCallback`, which is invoked when our element is appended to the dom.

</v-click>

---
layout: two-cols
clicks: 3
---

# Handle attributes

```ts {all|2|4-6|10-16}
class MokaPot extends HTMLElement {
  #servings = 1;

  static get observedAttributes() {
    return ['servings'];
  }

  // ...
  
  attributeChangedCallback(name, oldValue, newValue) {
    if (name === 'servings') {
      this.#servings = Number(newValue) || 1;

      this.cups.innerText = '☕️'.repeat(this.#servings);
    }
  }
}

```

::right::

Let's see how we can handle the `servings` attribute:

<v-clicks at="1">

- Define a private property to hold parsed value.
- Declare which attributes we want to keep track of.
- Handle `servings` attribute change, updating the private property and `cups`.

</v-clicks>

<style>
  .two-columns {
    gap: 1em;
    align-items: center;
  }
</style>

---
layout: two-cols
clicks: 2
---

# Handle state changes

```ts {all|3-11|12-15}
class MokaPot extends HTMLElement {
  //...
  makeCoffee() {
    this.replaceChildren(this.placeholder);

    const servings = Number(this.getAttribute('servings')) || 1;

    window.setTimeout(() => {
      this.replaceChildren(this.cups);
    }, 2000 * servings);
  }

  drinkCoffee() {
    this.replaceChildren(this.button);
  }
}

```

::right::

We can now implement the (really naïve) logic describing the component's behavior:

<v-clicks at="1">

- `makeCoffee` - Renders a placeholder, waits 2 seconds for each serving and then renders some cups of coffee.
- `drinkCoffee` - Resets our component to its inital state (displaying the **"Make Coffee"** button).

</v-clicks>

<style>
  .two-columns {
    gap: 1em;
    align-items: center;
  }
</style>

---
layout: two-cols
---

# Wire up interactions

```ts {3-14}
class MokaPot extends HTMLElement {
  // ...
  connectedCallback() {
    // ...
    this.button.addEventListener(
      "click",
      () => this.makeCoffee()
    );

    this.cups.addEventListener(
      "click",
      () => this.drinkCoffee()
    );
  }
  // ...
}

```

::right::

We now bind our logic to the `button` and the `cup` by adding event listeners.

> Note that we should also remove the listeners on `disconnectedCallback`, this is omitted for the sake of brevity.

<style>
  .two-columns {
    gap: 1em;
    align-items: center;
  }
</style>

---
layout: center
---

# Registering our custom element

Finally, we register our element in the `CustomElementRegistry`.

```ts
customElements.define("moka-pot", MokaPot);

```

---
layout: center
---

# The result

We can now use our component by simply adding `<moka-pot></moka-pot>` to the HTML

```html
<moka-pot servings="2"></moka-pot>
```

<moka-pot servings="2"></moka-pot>

<style>
  moka-pot {
    margin: 64px;
    font-size: 2em;
    text-align: center;
    display: block;
  }
</style>

---
layout: section
---

# Shadow DOM

---
layout: center
---

# Global styles are affecting our component

Notice how the global styles can affect how our component looks.

```css
  button {
    color: red;
  }
```

<MokaPotExample></MokaPotExample>

---
layout: center
clicks: 2
---

# Scoping our Custom Element
##

We can scope our element by attaching a shadow dom to it, this is how we do that:

```typescript {all|6|11}
class MokaPot extends HTMLElement {
  // ...
  constructor () {
    super();

    this.attachShadow({ mode: 'open' });
    // ...
  }
  // ...
  makeCoffee() {
    this.shadowRoot.replaceChildren(this.placeholder);
  }
}
```

<v-click at="2">

We also need to update our `replaceChildren` calls to target the `shadowRoot` instead of the element itself.

</v-click>

---
layout: center
---

# Problem solved

Styles are not bleeding in our component anymore

```css
  button {
    color: red;
  }
```

<moka-pot-shadow servings="2"></moka-pot-shadow>

> Note that the button looks really lame now because shared styles from the presentation itself cannot affect it anymore, all styles that can be inherited keep working though (for example font-family).

<style>
moka-pot-shadow {
  margin: 64px;
  font-size: 2em;
  text-align: center;
  display: block;
}
</style>

---
layout: center
clicks: 3
---

# It's not just about styling
##

The shadow DOM also allow us to define `id`s without having to worry about collisions with the rest of the document.

```typescript {all|4|10|15}
class MokaPot extends HTMLElement {
  constructor () {
    // ...
    this.button.id = 'make-coffee';
    // ...
  }
  // ...
  tellPeopleThatCapuccinoAfterLunchIsNotMorallyAcceptable() {
    // ...
    this.shadowRoot.getElementById('make-coffee') // button#make-coffee
    // ...
  }
}

document.getElementById('make-coffee'); // null
```

<v-clicks at="2">

- We can access the button in our `shadowRoot`.
- That cannot happen when querying the `document`

</v-clicks>

---
layout: section
---

# Templates

---
layout: center
---

# Setup a simple template for our component

Let's define a template for our moka component in our page html.

```html
<template>
  <style>
    header {
      color: yellow;
      text-align: center;
    }
    main {
      text-align: center;
    }
  </style>

  <header>
    <h3>Moka Pot</h3>
  </header>
  
  <main></main>

  <slot></slot>
</template>
```

Remember that this is not rendered in our page until we say so.

---
layout: two-cols
clicks: 5
---

# Inject a template in our component

```typescript {all|3|7-9|11|13|15}
export class MokaPot extends HTMLElement {
  // ...
  container!: HTMLElement | null;
  // ...
  connectedCallback() {
    // ...
    const template = document
      .getElementById('moka-template')
      ?.cloneNode(true) as HTMLTemplateElement;

    this.shadowRoot!.appendChild(template.content);

    this.container = this.shadowRoot!.querySelector('main');
    
    this.container?.replaceChildren(this.button);
  }
}
```

::right::

Let's grab our template and use it in our component.

<v-clicks at="1">

- Declare a property that will hold the content container
- Get the template from the `document` and clone it (deep clone)
- Append it to our shadow root
- Store the rendered container
- Render the initial state inside it 

</v-clicks>

<style>
  .two-columns {
    gap: 1em;
    align-items: center;
  }
</style>

---
layout: center
---

# Templated result

We can specify children on our `<moka-pot>`

```html
<moka-pot></moka-pot>
```

<moka-pot-template></moka-pot-template>

---
layout: center
clicks: 1
---

# Declaring slots

Notice how our template is specifying a `<slot>` element

```html {all|18}
<template>
  <style>
    header {
      color: yellow;
      text-align: center;
    }
    main {
      text-align: center;
    }
  </style>

  <header>
    <h3>Buongiorno</h3>
  </header>
  
  <main></main>

  <slot></slot>
</template>
```

<v-click at="1">

That indicates where child content should appear relatively to our `shadowRoot`.

</v-click>

---
layout: center
clicks: 1
---

# Using slots

We can specify children on our `<moka-pot>`

```html
<moka-pot>
  <p style="text-align: center;">I ❤️ ☕️</p>
</moka-pot>
```

<moka-pot-template>
  <p style="text-align: center;">I ❤️ ☕️</p>
</moka-pot-template>

<style>
  .two-columns {
    gap: 1em;
    align-items: center;
  }
</style>

---
layout: section
---

# Conclusions

---
layout: center
---

# Web Components Pros and Cons

## Pros
<v-clicks>

- Built in way to create components
- Efficient DOM management
- Do not depend upon a specific framework
- Style scoping

</v-clicks>

## Cons
<v-clicks>

- No easy way to render them server side (as of today)
- Not all frameworks have good support for web components

</v-clicks>
