export class MokaPot extends HTMLElement {
  #servings = 1;

  static get observedAttributes() {
    return ["servings"];
  }

  button = document.createElement("button");
  placeholder = document.createElement("span");
  cups = document.createElement("span");

  constructor() {
    super();

    this.attachShadow({ mode: "open" });

    this.button.id = "make-coffee";

    this.button.innerText = "Make Coffee";
    this.placeholder.innerText = "Making Coffee...";
    this.cups.innerText = "☕️";
  }

  connectedCallback() {
    this.button.addEventListener("click", () => this.makeCoffee(), {
      passive: true,
    });

    this.cups.addEventListener("click", () => this.drinkCoffee(), {
      passive: true,
    });

    this.shadowRoot!.replaceChildren(this.button);
  }

  attributeChangedCallback(name: string, oldValue: unknown, newValue: unknown) {
    if (name === "servings") {
      this.#servings = Number(newValue) || 1;

      this.cups.innerText = "☕️".repeat(this.#servings);
    }
  }

  makeCoffee() {
    this.shadowRoot!.replaceChildren(this.placeholder);

    setTimeout(() => {
      this.shadowRoot!.replaceChildren(this.cups);
    }, 2000 * this.#servings);
  }

  drinkCoffee() {
    this.shadowRoot!.replaceChildren(this.button);
  }
}

customElements.define("moka-pot-shadow", MokaPot);
