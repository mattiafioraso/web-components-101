export class MokaPot extends HTMLElement {
  constructor() {
    super();
    this.#servings = 1;
    this.button = document.createElement("button");
    this.placeholder = document.createElement("span");
    this.cups = document.createElement("span");
    this.attachShadow({ mode: "open" });
    this.button.innerText = "Make Coffee";
    this.placeholder.innerText = "Making Coffee...";
    this.cups.innerText = "\u2615\uFE0F";
  }
  #servings;
  static get observedAttributes() {
    return ["servings"];
  }
  connectedCallback() {
    this.button.addEventListener("click", () => this.makeCoffee(), {
      passive: true
    });
    this.cups.addEventListener("click", () => this.drinkCoffee(), {
      passive: true
    });
    this.shadowRoot.replaceChildren(this.button);
  }
  attributeChangedCallback(name, oldValue, newValue) {
    if (name === "servings") {
      this.#servings = Number(newValue) || 1;
      this.cups.innerText = "\u2615\uFE0F".repeat(this.#servings);
    }
  }
  makeCoffee() {
    this.shadowRoot.replaceChildren(this.placeholder);
    setTimeout(() => {
      this.shadowRoot.replaceChildren(this.cups);
    }, 2e3 * this.#servings);
  }
  drinkCoffee() {
    this.shadowRoot.replaceChildren(this.button);
  }
}
customElements.define("moka-pot", MokaPot);
